import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput} from 'react-native';
import io from 'socket.io-client';

export default class App extends Component {
  componentDidMount() {
    const socket = io('http://192.168.1.16:3000');
  }

  constructor(props) {
    super(props);
    this.state = {
      chatMessage: '',
    };
  }

  submitChatMessage = () => {
    this.socket.emit('chat message', this.state.chatMessage);
    this.setState({chatMessage: ''});
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>

        <View style={styles.container}>
          <TextInput
            style={{height: 40, borderWidth: 2}}
            autoCorrect={false}
            value={this.state.chatMessage}
            onSubmitEditing={this.submitChatMessage}
            onChangeText={chatMessage => {
              this.setState({chatMessage});
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
